﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = roleRepository;
        }

        /// <summary>
        ///     Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName
                }).ToList();
            return employeesModelList;
        }

        /// <summary>
        ///     Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        ///     Добавляет информацию о сотруднике в базу данных
        /// </summary>
        /// <param name="creationRequest">Информация о сотруднике</param>
        /// <returns>Результат выполнения запроса</returns>
        [HttpPost]
        [ProducesResponseType(201)]
        public async Task<IActionResult> AddEmployeeAsync(EmployeeCreationRequest creationRequest)
        {
            var employee = new Employee
            {
                AppliedPromocodesCount = 0,
                Email = creationRequest.Email,
                FirstName = creationRequest.FirstName,
                LastName = creationRequest.LastName,
                Id = Guid.NewGuid(),
                Roles = new List<Role>()
            };
            await _employeeRepository.AddAsync(employee);
            return Created($"{ControllerContext.HttpContext.Request.Path.ToString()}/{employee.Id.ToString()}",
                await GetEmployeeByIdAsync(employee.Id));
        }

        /// <summary>
        ///     Обновляет информацию о сотруднике в базу данных
        /// </summary>
        /// <param name="request">запрос на обновление информации</param>
        /// <returns>Результат обновления информации. Возвращает StatusCode 404, если такого сотрудника не существует</returns>
        [HttpPut]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest request)
        {
            var employee = await _employeeRepository.GetByIdAsync(request.Id);
            if (employee == null)
                return NotFound();
            employee.Email = request.Email;
            employee.FirstName = request.FirstName;
            employee.LastName = request.LastName;
            await _employeeRepository.UpdateAsync(employee);
            return Ok(await GetEmployeeByIdAsync(employee.Id));
        }

        /// <summary>
        ///     Удаляет сотрудника из базы данных
        /// </summary>
        /// <param name="id">Guid сотрудника</param>
        /// <returns>Результат выполнения запроса. Возвращает StatusCode 404, если сотрудника не существует</returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> RemoveEmployeeRequest(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            await _employeeRepository.RemoveAsync(employee);
            return Ok();
        }

        /// <summary>
        ///     Добавляет пользователю указанную роль
        /// </summary>
        /// <param name="employeeId">Id указанного пользователя</param>
        /// <param name="roleId">Id роли, которая добавляется пользователю</param>
        /// <returns></returns>
        [HttpPut("addRole")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> AddRoleToEmployee(Guid employeeId, Guid roleId)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee == null)
                return NotFound(new FailedAddRoleResponse
                {
                    Parameter = nameof(employeeId),
                    Result = "failed",
                    Message = $"There is no employee with id {employeeId.ToString()}"
                });
            var role = await _rolesRepository.GetByIdAsync(roleId);
            if (role == null)
                return NotFound(new FailedAddRoleResponse
                {
                    Parameter = nameof(roleId),
                    Result = "failed",
                    Message = $"There is no role with id {roleId.ToString()}"
                });
            employee.Roles.Add(role);
            return Ok();
        }
    }
}