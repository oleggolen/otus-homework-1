﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class EmployeeCreationRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }


        public string Email { get; set; }
    }
}