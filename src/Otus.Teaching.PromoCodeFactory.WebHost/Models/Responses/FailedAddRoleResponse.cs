﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class FailedAddRoleResponse
    {
        public string Result { get; set; }
        public string Parameter { get; set; }
        public string Message { get; set; }
    }
}