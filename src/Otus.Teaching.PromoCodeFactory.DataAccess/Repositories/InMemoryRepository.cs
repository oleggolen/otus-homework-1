﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }


        protected IList<T> Data { get; set; }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }


        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        public Task AddAsync(T item)
        {
            return Task.Run(() => Data.Add(item));
        }

        public Task UpdateAsync(T item)
        {
            return Task.Run(() =>
            {
                var index = Data.IndexOf(item);
                Data[index] = item;
            });
        }

        public Task RemoveAsync(T item)
        {
            return Task.Run(() => { Data.Remove(item); });
        }
    }
}